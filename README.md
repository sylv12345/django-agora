# Agora

Grande place publique dans la Grèce antique. Le but de se projet est de remettre cette place au gout du jour en créant un
site où les utulisateurs posent des questions sur des sujets divers, et d'autres utilisateurs peuvent y répondre.

## Mise en place VENV

```
$ source bin/activate
```

## Lancement projet

```
$ python manage.py  migrate 
```

```
$ python manage.py runserver
```

## Creation de l'utilisateur root pour /admin 
``` 
$ python manage.py createsuperuser 
```

## Panel admin

username : 'root'

password : 'root'

